using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointController : MonoBehaviour
{
    //public GameObject SaveMessage;
    private GameObject gameDataObject;
    private bool checkpointReached = false;
    void Start()
    {
        //SaveMessage.SetActive(false);
        gameDataObject = GameObject.Find("GameDataController");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Player" && !checkpointReached)
        {
            //StartCoroutine(SaveIconShowTime());
            gameDataObject.GetComponent<GameDataController>().SaveData();
            //gameDataObject.GetComponent<CheckpointDataController>().SaveData();
            //Destroy(gameObject);
            gameObject.SetActive(false);
        }
    }

    /*private IEnumerator SaveIconShowTime()
    {
        SaveMessage.SetActive(true);
        yield return new WaitForSeconds(3);
        SaveMessage.SetActive(false);
    }*/

}
