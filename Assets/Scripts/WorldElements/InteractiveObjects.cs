/*
 *  AUTHOR: Sandra
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveObjects : MonoBehaviour
{
    public InteractiveObjectFases interactiveObjectFases;
    private GameObject _player;
    public Weapon linkedWeapon;
    private GameObject ObjectInstance;

    public enum InteractiveObjectFases
    {
        Desactivat,
        Agafar,
        Equipat
    }

    // Start is called before the first frame update
    void Start()
    {
        interactiveObjectFases = InteractiveObjectFases.Desactivat;
        _player = GameObject.Find("PlayerController");
    }

    // Update is called once per frame
    void Update()
    {
        if (interactiveObjectFases == InteractiveObjectFases.Agafar) GrabObject();
        if (interactiveObjectFases == InteractiveObjectFases.Equipat) EquipObject();
    }

    void GrabObject()
    {
        // Agafa l'objecte i el jugador se l'equipa.
        if (_player.GetComponent<PlayerInventory>().ObjectInHand != null && _player.GetComponent<PlayerInventory>().ObjectInHand != this.gameObject)
        {
            _player.GetComponent<PlayerInventory>().ObjectInHand = null;     
            if (ObjectInstance != null) 
                Destroy(ObjectInstance);

        }
        
        _player.GetComponent<PlayerInventory>().ObjectInHand = linkedWeapon; 
        interactiveObjectFases = InteractiveObjectFases.Equipat;
        this.gameObject.layer = 2;
        
        //GameObject.Destroy(this.gameObject); // No es pot destruir, tambe desapareixera en les mans del jugador.
    }

    void EquipObject()
    {

        // Un cop agafat l'objecte el mante en una posicio determinada del jugador, com si el portes "equipat".
     
        GameObject attachmentSlot = _player.GetComponent<PlayerInventory>().AttachmentSlot;  
        if (attachmentSlot.transform.childCount != 0)
            GameObject.Destroy(attachmentSlot.transform.GetChild(0).gameObject);

        ObjectInstance = GameObject.Instantiate(linkedWeapon.WeaponPrefab, attachmentSlot.transform, false);
        _player.GetComponent<PlayerInventory>().Durability = linkedWeapon.Durability;
        ObjectInstance.transform.SetLocalPositionAndRotation(linkedWeapon.posOffset, linkedWeapon.rotateOffset);
        Destroy(this.gameObject);

    }
}
