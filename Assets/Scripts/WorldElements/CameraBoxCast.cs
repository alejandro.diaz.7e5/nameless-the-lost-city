/*
 *  AUTHOR: Sandra
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraBoxCast : MonoBehaviour
{
    private float maxDistanceRay = 5f;
    private RaycastHit hit;
    private bool isHit;
    public ElementDetectedInCast ElementDetected;
    [SerializeField] private GameObject _objectDetected;
    public Vector3 PositionHitObject;
    public InputManager _inputManager;

    private int _layerNum;
    
    public enum ElementDetectedInCast
    {
        Nothing,
        ObstacleToJump,
        ObstacleToBend,
        ObstacleToGrab,
        ObjectToGrab,
        ZipLine,
        Trampoline,
        Ladder
    }

    private void Awake()
    {
        _inputManager = new InputManager();
    }

    private void OnEnable()
    {
        _inputManager.Enable();
    }

    private void OnDisable()
    {
        _inputManager.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        isHit = Physics.BoxCast(transform.position, transform.lossyScale / 2, transform.forward, out hit, transform.rotation, maxDistanceRay);
        if (isHit)
        {
            ChangeElementDetected(hit.collider.gameObject);
            PositionHitObject = hit.point;
            //Debug.Log(PositionHitObject);
        }
        else ChangeElementDetected(null);

        if (_inputManager.Player.Interact.IsPressed()) InteractWithWorld();
    }

    private void OnDrawGizmos()
    {
        // Dibuixa el raig i cub per a veure'ls a l'escena.

        isHit = Physics.BoxCast(transform.position, transform.lossyScale /2, transform.forward, out hit, transform.rotation, maxDistanceRay);

        if (isHit)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.position, transform.forward * hit.distance);
            Gizmos.DrawWireCube(transform.position + transform.forward * hit.distance, transform.lossyScale);
        }
        else
        {
            Gizmos.color = Color.green;
            Gizmos.DrawRay(transform.position, transform.forward * maxDistanceRay);
        }
    }

    private void ChangeElementDetected(GameObject gameObject)
    {
        if (gameObject != null)
        {
            //Debug.Log(gameObject);
            switch (gameObject.gameObject.layer)
            {
                /*case 20: // 20 = Obstacle To Jump
                        ElementDetected = ElementDetectedInCast.ObstacleToJump;
                    break;

                case 21: // 21 = Obstacle To Bend
                    ElementDetected = ElementDetectedInCast.ObstacleToBend;
                    break;
                case 22: // 21 = Obstacle To Grab
                    ElementDetected = ElementDetectedInCast.ObstacleToGrab;
                    break;*/

                case 24: // 24 = Object To Grab
                    ElementDetected = ElementDetectedInCast.ObjectToGrab;
                    _layerNum = gameObject.gameObject.layer;
                    ActiveOutline(gameObject);
                    break;
                case 25: // 25 = ZipLine
                    Debug.Log("Layer 25");
                    ElementDetected = ElementDetectedInCast.ZipLine;
                    _layerNum = gameObject.gameObject.layer;
                    ActiveOutline(gameObject);
                    break;

                /*case 26: // 26 = Trampoline
                    ElementDetected = ElementDetectedInCast.Trampoline;
                    break;
                case 27: // 27 = Lader
                    ElementDetected = ElementDetectedInCast.Ladder;
                    break;*/

                case 19: // Mantener igual si layer = 19 (layer Outline)
                    Debug.Log("Layer 19, " + _layerNum);
                    break;
                default:
                    if (_objectDetected != null)
                    {
                        DesactiveOutline(_objectDetected, _layerNum);
                    }
                    ElementDetected = ElementDetectedInCast.Nothing;
                    break;
            }
        }
        else
        {
            ElementDetected = ElementDetectedInCast.Nothing;
            if (_objectDetected != null) DesactiveOutline(_objectDetected, _layerNum);
        }

        if (ElementDetected != ElementDetectedInCast.Nothing) _objectDetected = gameObject;
        
        
    }

    
    void ActiveOutline(GameObject gameObject)
    {
        //Debug.Log("Activar Outline");
        gameObject.gameObject.layer = 19;
    }

    void DesactiveOutline(GameObject gameObject, int layer)
    {
        //Debug.Log("Desactivar Outline");
        gameObject.gameObject.layer = layer;
    }


    void InteractWithWorld()
    {
        //Debug.Log("Interact");
        // Al premer "interactuar" far� cada objecte reaccionar.
        switch (ElementDetected)
        {
            case ElementDetectedInCast.ZipLine:
                _objectDetected.GetComponent<ZipLine>()._zipLineFases = ZipLine.ZipLineFases.DistanciaJugadorCuerda;
                break;
            case ElementDetectedInCast.ObjectToGrab:
                _objectDetected.GetComponent<InteractiveObjects>().interactiveObjectFases = InteractiveObjects.InteractiveObjectFases.Agafar;
                break;
        }
    }


}
