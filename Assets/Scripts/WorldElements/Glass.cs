using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glass : MonoBehaviour
{
    public GameObject brokenGlass;
    private float _minMagnitudeCollision = 2f;
    // Explosion efect
    private Vector3 _explosionPos;
    private Collider[] _colliders;
    private float _radius = 4f;
    private float _power = 10f;
    private float _upwards = 3f;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > _minMagnitudeCollision)
        {
            brokenGlass.transform.localScale = transform.localScale;
            Instantiate(brokenGlass, transform.position, transform.rotation);
            
            /*_explosionPos = transform.position;
            _colliders = Physics.OverlapSphere(_explosionPos, _radius);
            foreach (Collider hit in _colliders)
            {
                if (hit.attachedRigidbody)
                {
                    hit.attachedRigidbody.AddExplosionForce(_power * collision.relativeVelocity.magnitude, _explosionPos, _radius, _upwards);
                }
            }*/

            Destroy(this.gameObject);
        }
    }
}
