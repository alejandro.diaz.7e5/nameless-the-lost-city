/*
 *  AUTHOR: Javier
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderClimbing : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private Rigidbody rb;
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Ladder")
        {
            animator.SetBool("ladderclimb", true);
            Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical") * 2, 0f);
            rb.velocity = movement;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Ladder")
        {
            animator.SetBool("ladderclimb", false);
        }
    }
}
