/*
 *  AUTHOR: Sandra
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZipLine : MonoBehaviour
{
    /*
     *  ZipLine - Tirolina
     */

    [SerializeField] private GameObject _player;
    private Vector3 _zipLineEnd;
    private float _defaultSpeed = 7.5f;
    public ZipLineFases _zipLineFases;
    private InputManager _inputManager;
    private Vector3 _row;

    public enum ZipLineFases {
        Desactivada,
        DistanciaJugadorCuerda,
        MoviendoJugadorALaCuerda,
        MoviendoJugadorAlFinal
    }

    private void Awake()
    {
        _inputManager = new InputManager();
    }

    private void OnEnable()
    {
        _inputManager.Enable();
    }

    private void OnDisable()
    {
        _inputManager.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("PlayerController");
        _zipLineEnd = this.transform.GetChild(0).position;
            _zipLineEnd.y -= 1f;
        Debug.Log(_zipLineEnd);
        _zipLineFases = ZipLineFases.Desactivada;
    }

    // Update is called once per frame
    void Update()
    {
        if (_zipLineFases == ZipLineFases.DistanciaJugadorCuerda) DistancePlayerRow();
        
        if (_zipLineFases == ZipLineFases.MoviendoJugadorAlFinal) MovePlayerToEnd();
        else if (_zipLineFases == ZipLineFases.MoviendoJugadorALaCuerda) MovePlayerToRow();
    }

    private void MovePlayerToEnd()
    {
        // El jugador se deliza por la tirolina hasta llegar al final o se suelta a medio camino al volver a pulsar "interactuar"
        if ((_player.transform.position.x == _zipLineEnd.x && _player.transform.position.z == _zipLineEnd.z) || _inputManager.Player.Interact.IsPressed())
        {
            _zipLineFases = ZipLineFases.Desactivada;
            _player.transform.GetComponent<Rigidbody>().useGravity = true;
        }
        else
        {
            _player.transform.position = Vector3.MoveTowards(_player.transform.position, _zipLineEnd, _defaultSpeed * Time.deltaTime);
        }
    }

    private void MovePlayerToRow()
    {
        // Mueve al jugador hasta la tirolina, donde el rayo chocaba con la cuerda
        
        //Debug.Log("Gravity off");
        _player.transform.GetComponent<Rigidbody>().useGravity = false;
        _player.transform.position = Vector3.MoveTowards(_player.transform.position, _row, _defaultSpeed * Time.deltaTime);
           
        if ((_player.transform.position.x == _row.x && _player.transform.position.y == _row.y) /*|| _inputManager.Player.Interact.IsPressed()*/) _zipLineFases = ZipLineFases.MoviendoJugadorAlFinal;
        
    }

    private void DistancePlayerRow()
    {
        // Mira la posicion donde el rayo choca con la tirolina
        _row = GameObject.Find("Camera").GetComponent<CameraBoxCast>().PositionHitObject;
            _row.y -= 1f;
        _zipLineFases = ZipLineFases.MoviendoJugadorALaCuerda;
    }
}
