/*
 *  AUTHOR: Javier
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSlide : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private Rigidbody rb;
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "GroundSlideAble")
        {
            //animator.SetBool("ladderclimb", true);
            
            if (Input.GetKey(KeyCode.C))
            {
                animator.SetTrigger("slide");
                Debug.Log("ENTR�");
                Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
                rb.velocity = movement;
            }
        }
    }

    /*private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "GroundSlideAble")
        {
            animator.SetTrigger("ladderclimb", false);
        }
    }*/
}
