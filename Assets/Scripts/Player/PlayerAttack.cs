using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public PlayerInventory inventory;
    private bool AttackInCD;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !AttackInCD)
        {
            Debug.Log("Attacking");
            Attack();
            StartCoroutine("AttackCD");
        }
    }
    void Attack()
    {
        Collider[] targets = Physics.OverlapBox(transform.position + transform.forward*1, new Vector3(0.5f, 0.5f, 0.5f));
        
        GetComponent<Animator>().SetTrigger(inventory.ObjectInHand == null ? "punch" : "WeaponAttack");
        if (inventory.ObjectInHand != null)
            StartCoroutine(PlaySound());
        if (targets != null)
        {
            foreach (Collider target in targets)
            {
                if (target.gameObject.tag == "Enemy" && !target.isTrigger)
                {
                    Debug.Log("Damaging enemy");
                    target.gameObject.GetComponent<EnemyDamage>().TakeDamage(inventory.ObjectInHand == null ? 1 : inventory.ObjectInHand.Damage);
                    if (inventory.ObjectInHand != null) inventory.Use();
                }
            }
        }
        else
        {
            Debug.Log("No target detected");
        }
    }

    private static IEnumerator PlaySound()
    {
        yield return new WaitForSeconds(0.5f);
        GameObject.Find("WeaponSlot").GetComponent<AudioSource>().Play();
    }
    IEnumerator AttackCD()
    {
        AttackInCD = true;
        yield return new WaitForSeconds(inventory.ObjectInHand == null ? 1f : 2f);
        AttackInCD = false;
    }
}
