
/*
 *  AUTHOR: Alejandro
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDamage : MonoBehaviour
{
    
    public PlayerData player;
    public Image bgImg;
    public Image defeatImg;
    public float HitCD;
    public bool HitFlag;
    public AudioClip hitClip;
    public AudioClip deathClip;
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log($"Collision {collision.gameObject.tag} detected");
        if (collision.gameObject.tag == "Weapon" && !HitFlag)
        {
            player.Life -= 1;
            if (player.Life <= 0)
                Die();
            else
            {
                HitFlag = true;
                StartCoroutine(HitCooler());
                StartCoroutine(HitAnim());
            }
        }

    }
    private IEnumerator HitCooler()
    {
        yield return new WaitForSeconds(HitCD);
        HitFlag = false;
        yield return null;        
    }
    private IEnumerator HitAnim()
    {
        GetComponent<AudioSource>().PlayOneShot(hitClip);
        GetComponent<AudioSource>().time = 1;
        Color tempColor = Color.red;      
        float alphaValue = 1;
        while (alphaValue > 0)
        {
            tempColor.a = alphaValue;
            alphaValue -= 0.2f;
            bgImg.color = tempColor;
            yield return new WaitForSeconds(0.05f);
        }
        yield return null;
    }
    private void Die()
    {
        defeatImg.gameObject.SetActive(true);
        GetComponent<Animator>().SetTrigger("Die");
        GetComponent<AudioSource>().PlayOneShot(deathClip);
        GetComponent<AudioSource>().time = 0.5f;
        GetComponent<PlayerController>().rbfps.enabled = false;
        GetComponent<PlayerController>().enabled = false;
        GetComponent<CapsuleCollider>().enabled = false;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
    }
    public void DirectDamage(float damage)
    {
        if (!HitFlag)
        {
            player.Life -= damage;
            if (player.Life <= 0)
                Die();
            HitFlag = true;
            StartCoroutine(HitCooler());
            StartCoroutine(HitAnim());
        }
    }
}

