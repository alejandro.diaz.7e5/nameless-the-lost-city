/*
 *  AUTHOR: Sandra
 */

using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    public Weapon ObjectInHand;
    public GameObject AttachmentSlot;
    public GameObject ObjectInstance;
    public int Durability;
    // Start is called before the first frame update
    void Start()
    {
        if (ObjectInHand != null) AttachWeaponToSlot();
    }

    private void AttachWeaponToSlot()
    {
        if (AttachmentSlot.transform.childCount != 0)
            GameObject.Destroy(AttachmentSlot.transform.GetChild(0));
        ObjectInstance = GameObject.Instantiate(ObjectInHand.WeaponPrefab, AttachmentSlot.transform, false);
        ObjectInstance.transform.localPosition = ObjectInHand.posOffset;
        ObjectInstance.transform.SetLocalPositionAndRotation(ObjectInHand.posOffset, ObjectInHand.rotateOffset);

    }

    // Update is called once per frame
    public void Use()
    {
        Durability--;
        if (Durability <= 0)
        {
            GameObject.Destroy(AttachmentSlot.transform.GetChild(0).gameObject);
            ObjectInHand = null;
        }
    }



}
