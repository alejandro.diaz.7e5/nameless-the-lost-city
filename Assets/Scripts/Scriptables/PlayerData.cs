using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "Scriptables/Player Data")]
public class PlayerData : ScriptableObject, IBootableScript
{
    public float Life;
    void IBootableScript.Boot()
    {
        Debug.Log("Booting");
        Life = 5;
    }
}
