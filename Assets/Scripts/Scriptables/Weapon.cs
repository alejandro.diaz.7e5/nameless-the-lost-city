using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New weapon", menuName = "Scriptables/Weapon")]
public class Weapon : ScriptableObject
{
    public GameObject WeaponPrefab;
    public float Damage;
    public int Durability;
    public Vector3 posOffset;
    public Quaternion rotateOffset;
}
