using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "SceneData", menuName = "Scriptables/SceneData")]
public class SceneData : ScriptableObject
{
    public string LastScene;

    private void OnEnable() // Per a que no es reinici al recaregar l'escena.
    {
        hideFlags = HideFlags.DontUnloadUnusedAsset;
    }
}
