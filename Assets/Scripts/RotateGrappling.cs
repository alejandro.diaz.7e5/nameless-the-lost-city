using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateGrappling : MonoBehaviour
{
    public Grappling grappling;
    void Update()
    {
        if (!grappling.IsGrappling()) return;
        transform.LookAt(grappling.GetGrapplePoint());
    }
}
