using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableBoot : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    public List<ScriptableObject> bootableScripts;
    void Start()
    {
        foreach (IBootableScript script in bootableScripts)
            script.Boot();
    }
}
