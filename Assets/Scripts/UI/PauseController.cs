using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets.Characters.FirstPerson
{
    public class PauseController : MonoBehaviour
    {
        [SerializeField] private GameObject pauseScreen;
        private GameObject WinController;
        [SerializeField] private bool inPause;
        private GameObject Timer;
        [SerializeField] private GameObject textoTimer;

        void Start()
        {
            Time.timeScale = 1;
            inPause = false;
            //pauseScreen = GameObject.Find("TimeController");
            WinController = GameObject.Find("WinCollider");
            Timer = GameObject.Find("TimeController");
            pauseScreen.SetActive(false);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = false;
        }

        void Update()
        {
            if (WinController.GetComponent<GameWinController>().GameWin == false)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    if (inPause)
                    {
                        Cursor.lockState = CursorLockMode.Confined;
                        Cursor.visible = false;
                        pauseScreen.SetActive(false);
                        inPause = false;
                        Time.timeScale = 1;
                    }
                    else
                    {
                        Cursor.lockState = CursorLockMode.Confined;
                        Cursor.visible = true;
                        textoTimer.gameObject.GetComponent<Text>().text = Timer.GetComponent<Timer>().TimeToString;
                        pauseScreen.SetActive(true);
                        inPause = true;
                        Time.timeScale = 0;
                    }
                }
            }
        }

        public void Resume()
        {
            pauseScreen.SetActive(false);
            inPause = false;
            Time.timeScale = 1;
        }
    }

}

