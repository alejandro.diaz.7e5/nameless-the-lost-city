using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    public void SceneLoader(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    public void ExitApplication()
    {
        Application.Quit();
    }

    public void LoadMenu()
    {
        SceneLoader("Menu_Scene");
    }

    public void LoadPause()
    {
        SceneLoader("Pausa_Scene");
    }

    public void LoadLevelSelector()
    {
        SceneLoader("SelectorNiveles_Scene");
    }

    public void LoadSkills()
    {
        SceneLoader("Habilidades_Scene");
    }

    public void LoadConfiguration()
    {
        SceneLoader("Configuracion_Scene");
    }

    public void GoBack()
    {
        SceneLoader(Resources.Load<SceneData>("SceneData").LastScene);
    }
}
