using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UiManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //if (SceneManager.GetActiveScene().name == "Menu_Scene")
        if ( SceneManager.GetActiveScene().name != "Configuracion_Scene" &&
             SceneManager.GetActiveScene().name != "Habilidades_Scene" )
            Resources.Load<SceneData>("SceneData").LastScene = SceneManager.GetActiveScene().name;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
