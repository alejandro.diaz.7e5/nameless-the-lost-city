using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutController : MonoBehaviour
{
    public GameObject TutMessage;
    private bool messageSpawned = false;
    void Start()
    {
        TutMessage.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Player" && !messageSpawned)
        {
            TutMessage.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        TutMessage.SetActive(false);
        messageSpawned = true;
    }
}
