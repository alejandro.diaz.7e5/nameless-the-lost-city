/*
 *  AUTHOR: Alejandro
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarUpdater : MonoBehaviour
{
    public PlayerData data;
    RectTransform bar;
    public float baseWidth;
    public float height;

    // Start is called before the first frame update
    void Start()
    {
        bar = gameObject.GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        bar.sizeDelta = new Vector2(baseWidth*data.Life, height);
    }
}
