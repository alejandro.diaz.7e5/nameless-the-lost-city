using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBoost : MonoBehaviour
{
    private GameObject player;
    private Rigidbody PlayeRb;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        PlayeRb = player.GetComponent<Rigidbody>();

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (Input.GetAxisRaw("Horizontal") > 0)
            {
                PlayeRb.velocity = new Vector3(20f, PlayeRb.velocity.y, PlayeRb.velocity.z);
            }
            else if (Input.GetAxisRaw("Vertical") > 0)
            {
                PlayeRb.velocity = new Vector3(PlayeRb.velocity.x, PlayeRb.velocity.y, 20f);
            }
            //PlayeRb.velocity = new Vector3(20f, PlayeRb.velocity.y, 20f);
        }
    }
}
