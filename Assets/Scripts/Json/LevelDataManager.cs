using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelDataManager : MonoBehaviour
{
    public Button [] levelbuttons;
    public string fileName;
    private string saveFile;
    public MenuData menuData = new MenuData();

    private void Awake()
    {
        saveFile = Application.dataPath + "/" + fileName + ".json";
        LoadAvailableLevels();
    }

    public void LoadAvailableLevels()
    {
        if (File.Exists(saveFile))
        {
            string content = File.ReadAllText(saveFile);
            menuData = JsonUtility.FromJson<MenuData>(content);

            if (menuData.lvl2Unlocked)
            {
                levelbuttons[1].interactable = true;
            }
            else
            {
                levelbuttons[1].interactable = false;
            }
        }
        else
        {
            Debug.Log("File doesn't exists");
        }
    }
}
