using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public Vector3 position;
    public float savedMinut;
    public float savedSeconds;
}
