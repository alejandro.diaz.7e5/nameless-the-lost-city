using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class MenuData
{
    public bool lvl1Unlocked;
    public bool lvl2Unlocked;
}
