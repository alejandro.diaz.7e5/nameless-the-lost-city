using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuDataManager : MonoBehaviour
{
    public Button continueButton;
    public string lvl1FileName, lvl2FileName, levelsFileName;
    private string saveFile;
    public MenuData menuData = new MenuData();

    private void Awake()
    {
        saveFile = Application.dataPath + "/";
        CheckLevelsFile();
    }

    public void CheckLevelsFile()
    {
        if (File.Exists(saveFile + levelsFileName + ".json"))
        {
            continueButton.interactable = true;
        }
        else
        {
            continueButton.interactable = false;
        }
    }

    public void CreateNewGame()
    {
        MenuData newData = new MenuData()
        {
            lvl1Unlocked = true,
            lvl2Unlocked = false

        };

        string cadenaJSON = JsonUtility.ToJson(newData);
        File.WriteAllText(saveFile + levelsFileName + ".json", cadenaJSON);

        Debug.Log("Archivo guardado");

        if (File.Exists(saveFile + lvl1FileName + ".json"))
        {
            File.Delete(saveFile + lvl1FileName + ".json");
        }

        if (File.Exists(saveFile + lvl2FileName + ".json"))
        {
            File.Delete(saveFile + lvl2FileName + ".json");
        }

        SceneManager.LoadScene("SelectorNiveles_Scene");
    }


    public void CheckLevelContinueSaveFile()
    {
        if (File.Exists(saveFile + lvl1FileName + ".json") && !File.Exists(saveFile + lvl2FileName + ".json"))
        {
            SceneManager.LoadScene("MapLvl1");
        }
        else if(!File.Exists(saveFile + lvl1FileName + ".json") && File.Exists(saveFile + lvl2FileName + ".json"))
        {
            SceneManager.LoadScene("MapLvl2");
        }
        else
        {
            SceneManager.LoadScene("SelectorNiveles_Scene");
        }
    }
}
