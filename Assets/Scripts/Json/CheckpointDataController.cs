using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class CheckpointDataController : MonoBehaviour
{
    public string saveCheckpoint;
    public CheckpointData checkpointData = new CheckpointData();
    public GameObject[] Checkpoints;
    public bool[] checkponintEnabled;

    private void Awake()
    {
        Debug.Log("CHECKPOINTDATACONTROLLER AWAKE");
        saveCheckpoint = Application.dataPath + "/Scripts/Json/checkpoints.json";
        LoadData();
        for (int i = 0; i< checkponintEnabled.Length; i++)
        {
            if (!checkponintEnabled[i])
            {
                Checkpoints[i].SetActive(false);
            }
        }
    }

    private void Start()
    {
        //LoadData();
    }

    void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.C))
        {
            LoadData();
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            SaveData();
        }*/
    }

    public void LoadData()
    {
        if (File.Exists(saveCheckpoint))
        {
            //Debug.Log("CARGANDO CHECKPOINTS");
            string content = File.ReadAllText(saveCheckpoint);
            checkpointData = JsonUtility.FromJson<CheckpointData>(content);

            checkponintEnabled = checkpointData.checkpoints;
        }
        else
        {
            Debug.Log("File doesn't exists");
        }
    }

    public void SaveData()
    {
        CheckpointData newData = new CheckpointData()
        {
            checkpoints = checkponintEnabled

        };

        string cadenaJSON = JsonUtility.ToJson(newData);

        File.WriteAllText(saveCheckpoint, cadenaJSON);

        Debug.Log("Archivo guardado");
    }

    public void DeleteData()
    {
        if (File.Exists(saveCheckpoint))
        {
            File.Delete(saveCheckpoint);
        }
        else
        {
            Debug.Log("File doesn't exists");
        }
    }
}
