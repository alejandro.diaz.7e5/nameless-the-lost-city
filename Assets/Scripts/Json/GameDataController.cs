using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class GameDataController : MonoBehaviour
{
    private GameObject player;
    public string fileName;
    public string saveFile;
    public GameData gameData = new GameData();
    public GameObject SaveMessage;
    public GameObject LoadMessage;
    private GameObject timer;
    public string ActualSeneName;

    private void Awake()
    {
        timer = GameObject.Find("TimeController");
        saveFile = Application.dataPath + "/" + fileName + ".json";
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Start()
    {
        Debug.Log("GAMEDATACONTROLLER START");
        SaveMessage.SetActive(false);
        LoadMessage.SetActive(false);
        LoadData();
    }

    void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.C))
        {
            LoadData();
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            SaveData();
        }*/
    }

    public void LoadData()
    {
        if (File.Exists(saveFile))
        {
            //StartCoroutine(SaveIconShowTime());

            string content = File.ReadAllText(saveFile);
            gameData = JsonUtility.FromJson<GameData>(content);

            Debug.Log("Posicion Jugador: " + gameData.position);

            player.transform.position = gameData.position;
            timer.GetComponent<Timer>().minut = gameData.savedMinut;
            timer.GetComponent<Timer>().seconds = gameData.savedSeconds;
        }
        else
        {
            Debug.Log("File doesn't exists");
        }
    }

    public void SaveData()
    {
        StartCoroutine(SaveIconShowTime());

        GameData newData = new GameData()
        {
            position = player.transform.position,
            savedMinut = timer.GetComponent<Timer>().minut,
            savedSeconds = timer.GetComponent<Timer>().seconds

        };

        string cadenaJSON = JsonUtility.ToJson(newData);

        File.WriteAllText(saveFile, cadenaJSON);

        Debug.Log("Archivo guardado");
    }

    public void DeleteData()
    {
        if (File.Exists(saveFile))
        {
            File.Delete(saveFile);
        }
        else
        {
            Debug.Log("File doesn't exists");
        }
    }

    public void SaveUnlockables()
    {
        MenuData newData = new MenuData()
        {
            lvl1Unlocked = true,
            lvl2Unlocked = true

        };

        string cadenaJSON = JsonUtility.ToJson(newData);
        File.WriteAllText(Application.dataPath + "/saveFileLevels.json", cadenaJSON);
    }

    public void LimitReload()
    {
        if (File.Exists(saveFile))
        {
            string content = File.ReadAllText(saveFile);
            gameData = JsonUtility.FromJson<GameData>(content);

            Debug.Log("Posicion Jugador: " + gameData.position);

            player.transform.position = gameData.position;
            timer.GetComponent<Timer>().minut = gameData.savedMinut;
            timer.GetComponent<Timer>().seconds = gameData.savedSeconds;
        }
        else
        {
            SceneManager.LoadScene(ActualSeneName);
        }
    }

    public void LevelRestart()
    {
        DeleteData();
        SceneManager.LoadScene(ActualSeneName);
    }

    private IEnumerator SaveIconShowTime()
    {
        SaveMessage.SetActive(true);
        yield return new WaitForSeconds(3);
        SaveMessage.SetActive(false);
    }

    private IEnumerator LoadIconShowTime()
    {
        LoadMessage.SetActive(true);
        yield return new WaitForSeconds(3);
        LoadMessage.SetActive(false);
    }
}
