using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
public class JumpBoost : MonoBehaviour
{
    private GameObject player;
    private Rigidbody PlayeRb;
    [SerializeField] private float jumpImpulse;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        PlayeRb = player.GetComponent<Rigidbody>();

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PlayeRb.velocity = new Vector3(PlayeRb.velocity.x, jumpImpulse, PlayeRb.velocity.z);
        }
    }
}
