using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Grappling : MonoBehaviour
{
    private LineRenderer lr;
    private Vector3 grapplePoint;
    public LayerMask whatIsGrappleable;
    public Transform gunTip, camera, player;
    private float maxDistance = 100f;
    private SpringJoint joint;
    public PlayerIK antonioIK;
    public Animator playerAnimator;

    void Awake()
    {
        lr = GetComponent<LineRenderer>();
        playerAnimator = GameObject.FindObjectOfType<PlayerController>().playerAnimator;
    }

    void Update()
    {
        DrawRope();
        if (Input.GetMouseButtonDown(1))
        {
            StartGrapple();
        }
        else if (Input.GetMouseButtonUp(1))
        {
            StopGrapple();
        }
    }
    void LateUpdate()
    {
        DrawRope();
    }

    void StartGrapple()
    {
        if (!playerAnimator.gameObject.GetComponent<RigidbodyFirstPersonController>().IsGrounded) playerAnimator.SetBool("Grappling", true);
        RaycastHit hit;
        if (Physics.Raycast(camera.position, camera.forward, out hit, maxDistance, whatIsGrappleable))
        {
            grapplePoint = hit.point;
            joint = player.gameObject.AddComponent<SpringJoint>();
            joint.autoConfigureConnectedAnchor = false;
            joint.connectedAnchor = grapplePoint;

            float distanceFromPoint = Vector3.Distance(player.position, grapplePoint);

            joint.maxDistance = distanceFromPoint * 0.8f;
            joint.minDistance = distanceFromPoint * 0.25f;

            joint.spring = 4.5f;
            joint.damper = 7f;
            joint.massScale = 4.5f;

            lr.positionCount = 2;
            antonioIK.target = hit.point;
            antonioIK.ikActive = true;
        }
    }

    void StopGrapple()
    {
        playerAnimator.SetBool("Grappling", false);
        lr.positionCount = 0;
        Destroy(joint);
        antonioIK.ikActive = false;
    }

    void DrawRope()
    {
        if (!joint) return;
        lr.SetPosition(0, gunTip.position);
        lr.SetPosition(1, grapplePoint);
    }

    public bool IsGrappling()
    {
        return joint != null;
    }

    public Vector3 GetGrapplePoint()
    {
        return grapplePoint;
    }
    
}
