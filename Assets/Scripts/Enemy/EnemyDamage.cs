using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{
    public EnemyData enemy;
    public EnemyList list;
    public AudioClip hitSound;
    public AudioClip deathSound;

    public void TakeDamage(float damage)
        {
        StartCoroutine(DamageWaitTime(damage));
        
     }
    private void Die()
    {
        GetComponent<Animator>().SetTrigger("Die");
        GetComponent<SphereCollider>().enabled = false;
        GetComponent<CapsuleCollider>().enabled = false;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        GetComponent<AudioSource>().PlayOneShot(deathSound);
    }

    IEnumerator DamageWaitTime(float damage)
    {
        yield return new WaitForSeconds(1f);
        if (list.DamageEnemy(enemy.ID, damage) <= 0)
            Die();
        else
        {
            GetComponent<AudioSource>().PlayOneShot(hitSound);
            GetComponent<AudioSource>().time = 0.5f;
        }
    }
}
