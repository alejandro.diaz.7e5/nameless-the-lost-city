using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "EnemyList", menuName = "Scriptables/Enemy List")]
public class EnemyList : ScriptableObject, IBootableScript
{
    public float asd;
    [SerializeField]
    public List<Enemy> enemies;
    [System.Serializable]
    public struct Enemy
    {
        public float Life;
        public int ID;
    }

    void IBootableScript.Boot()
    {
        enemies.Clear();
        EnemyData[] Data = FindObjectsOfType<EnemyData>();
        foreach (EnemyData foundEnemy in Data)
        {
            foundEnemy.GetComponent<EnemyDamage>().list = this;
            Enemy enemy;
            enemy.Life = foundEnemy.Life;
            enemy.ID = foundEnemy.ID;
            enemies.Add(enemy);
        }
    }
    public float DamageEnemy(int id, float damage)
    {
        int TargetPos = enemies.FindIndex(x => x.ID == id);
        Enemy temp = enemies[TargetPos];
        temp.Life -= damage;
        enemies[TargetPos] = temp;
        return enemies[TargetPos].Life;
    }

}
