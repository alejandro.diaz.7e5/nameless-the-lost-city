
/*
 *  AUTHOR: Alejandro
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public Animator EnemyAnimator;
    public Rigidbody rb;
    public Collider thisCollider;
    public Collider weaponCollider;
    public Coroutine AttackCD;
    public bool AttackFlag;
    private float fallOverride;
    // Start is called before the first frame update
    void Start()
    {
        Physics.IgnoreCollision(thisCollider, weaponCollider);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerStay(Collider collision)
    {

        if (collision.gameObject.tag == "Player")
            if (CalculatePlayerDistance(collision.gameObject) < 2 && !AttackFlag)
            {
                AttackFlag = true;
                StartCoroutine(AttackEvents());
                Attack();
            }
            else
                RunToPlayer(collision.gameObject);



    }
    private void OnTriggerExit(Collider collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            EnemyAnimator.SetBool("Walking", false);
            rb.velocity = Vector3.zero;
        }

    }

    private float CalculatePlayerDistance(GameObject player)
    {
        Vector3 distance = transform.position - player.transform.position;
        return distance.magnitude;
    }
    private void Attack() 
    {
        EnemyAnimator.SetTrigger("Attack");
    }
    private void RunToPlayer(GameObject player)
    {
        transform.LookAt(player.transform);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, transform.eulerAngles.z);
        EnemyAnimator.SetBool("Walking", true);
        fallOverride = rb.velocity.y;
        Vector3 move = (player.transform.position - transform.position).normalized * 3;
        move.y = fallOverride;
        rb.velocity = move;

    }
    private IEnumerator AttackEvents()
    {
        yield return new WaitForSeconds(.5f);
        weaponCollider.enabled = true;
        yield return new WaitForSeconds(.5f);
        weaponCollider.enabled = false;
        yield return null;
    }
 
}

