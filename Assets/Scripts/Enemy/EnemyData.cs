using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyData : MonoBehaviour
{
   
    public int ID;
    public float Life;
    public float Damage;
}
