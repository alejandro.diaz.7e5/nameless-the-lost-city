/*
 *  AUTHOR: Javier
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameWinController : MonoBehaviour
{
    [SerializeField] private GameObject winScreen;
    [SerializeField] private GameObject gameScreen;
    [SerializeField] private Sprite[] medalsArray;
    private GameObject timer;
    [SerializeField] private RawImage medal;
    [SerializeField] private float goldMinut;
    [SerializeField] private float goldSeconds;
    [SerializeField] private float silverMinut;
    [SerializeField] private float silverSeconds;
    private GameObject gameDataController;

    public bool GameWin = false;
    void Start()
    {
        winScreen.gameObject.SetActive(false);
        timer = GameObject.Find("TimeController");
        gameDataController = GameObject.Find("GameDataController");
        gameScreen = GameObject.Find("GamePanel");
    }
    void Update()
    {
        if (GameWin)
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameWin = true;
            
            gameScreen.gameObject.SetActive(false);
            winScreen.gameObject.SetActive(true);
            if (timer.GetComponent<Timer>().minut <= goldMinut)
            {
                if (timer.GetComponent<Timer>().minut == goldMinut && timer.GetComponent<Timer>().seconds <= goldSeconds)
                {
                    medal.texture = medalsArray[2].texture;
                }else if (timer.GetComponent<Timer>().minut < goldMinut)
                {
                    medal.texture = medalsArray[2].texture;
                }
                
            }
            
            if (timer.GetComponent<Timer>().minut <= silverMinut && timer.GetComponent<Timer>().minut >= goldMinut)
            {
                if (timer.GetComponent<Timer>().minut == silverMinut && timer.GetComponent<Timer>().seconds <= silverSeconds)
                {
                    medal.texture = medalsArray[1].texture;
                }
                else if (timer.GetComponent<Timer>().minut < silverMinut)
                {
                    medal.texture = medalsArray[1].texture;
                }
            }

            if(timer.GetComponent<Timer>().minut >= silverMinut)
            {
                if (timer.GetComponent<Timer>().minut == silverMinut && timer.GetComponent<Timer>().seconds > silverSeconds)
                {
                    medal.texture = medalsArray[0].texture;
                }
                else if (timer.GetComponent<Timer>().minut > silverMinut)
                {
                    medal.texture = medalsArray[0].texture;
                }
            }
        }
        gameDataController.GetComponent<GameDataController>().DeleteData();
        gameDataController.GetComponent<GameDataController>().SaveUnlockables();
        //gameDataController.GetComponent<CheckpointDataController>().DeleteData();
        StartCoroutine(FreezeScreenTime());
    }

    private IEnumerator FreezeScreenTime()
    {
        yield return new WaitForSeconds(2);
        Time.timeScale = 0;
    }

}
