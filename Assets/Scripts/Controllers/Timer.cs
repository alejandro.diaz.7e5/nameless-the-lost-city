using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    public float minut;
    public float seconds;
    private string minut0, second0;
    private GameObject textoTimer;
    public PlayerData player;
    private GameObject GameWin;
    public string TimeToString;

    private void Start()
    {
        GameWin = GameObject.Find("WinCollider");
    }

    void Update()
    {
        if (player.Life > 0 && !GameWin.GetComponent<GameWinController>().GameWin)
        {
            seconds += Time.deltaTime;
            if (seconds > 59f)
            {
                if (minut >= 23 || minut < 0)
                {
                    minut = 0;
                }
                else
                {
                    minut += 1;
                }
                seconds = 0;
            }

            if (minut < 10)
            {
                minut0 = "0";
            }
            else
            {
                minut0 = "";
            }

            if (seconds < 9.5f)
            {
                second0 = "0";
            }
            else
            {
                second0 = "";
            }
        }

        textoTimer = GameObject.Find("TimeText");
        textoTimer.gameObject.GetComponent<TMP_Text>().text = minut0 + minut.ToString("f0") + ":" + second0 + seconds.ToString("f0");
        TimeToString = minut0 + minut.ToString("f0") + ":" + second0 + seconds.ToString("f0");
    }
}
