using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapLimit : MonoBehaviour
{
    private GameObject gameDataObject;
    void Start()
    {
        gameDataObject = GameObject.Find("GameDataController");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            gameDataObject.GetComponent<GameDataController>().LimitReload();
        }
    }
}
