using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIK : MonoBehaviour
{
    public Animator playerAnim;
    public bool ikActive;
    public Vector3 target;

    [SerializeField]
    private float currentWeight;
    private float currentvelocity;
    private float newWeight;
    private void Start()
    {
        playerAnim = GetComponent<Animator>();
    }
    void OnAnimatorIK()
    {
     
            currentWeight = Mathf.SmoothDamp(currentWeight, ikActive ? 1 : 0, ref currentvelocity, 0.3f);
            playerAnim.SetIKPositionWeight(AvatarIKGoal.LeftHand, currentWeight);
            playerAnim.SetIKPosition(AvatarIKGoal.LeftHand, target);
        
    }
}
